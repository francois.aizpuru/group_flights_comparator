import axios from "axios";
import rateLimit from "axios-rate-limit";
import secrets from "../secrets.json";

const flightAPIKey = secrets.flight.apikey;

const skyscannerHttp = rateLimit(axios.create(), {
  maxRequests: 50,
  perMilliseconds: 1000,
  maxRPS: 1,
});

async function getAirports(search) {
  let url =
    "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/FR/EUR/fr-FR/?query=" +
    search;
  let headers = {
    "x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
    "x-rapidapi-key": flightAPIKey,
    useQueryString: true,
  };

  let result = await skyscannerHttp({
    method: "GET",
    url: url,
    headers: headers,
  });
  let places = result.data.Places;
  let resultList = places.map(
    (el) =>
      el.PlaceName +
      " (" +
      el.PlaceId.replace("-sky", "") +
      "-" +
      el.CountryId.replace("-sky", "") +
      ")"
  );
  return resultList;
}

async function getQuotes(airport, period) {
  let result = await skyscannerHttp({
    method: "GET",
    url:
      "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsequotes/v1.0/FR/EUR/fr-FR/" +
      airport +
      "/anywhere/" +
      period[0] +
      "/" +
      period[1],
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host":
        "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
      "x-rapidapi-key": flightAPIKey,
      useQueryString: true,
    },
  })
    .then((response) => {
      let result = response.data.Quotes.map((quote) => {
        return {
          minPrice: quote.MinPrice,
          direct: quote.Direct,
          destination: response.data.Places.filter(
            (place) => quote.OutboundLeg.DestinationId == place.PlaceId
          )[0],
        };
      });
      return result;
    })
    .catch((error) => {
      console.log(error);
    });
  return result;
}

export default { getAirports, getQuotes };
