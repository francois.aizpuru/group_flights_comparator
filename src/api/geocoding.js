import axios from "axios";
import rateLimit from "axios-rate-limit";
import secrets from "../secrets.json";

const geocodingApiKey = secrets.geocoding.apikey;
const geocodingHttp = rateLimit(axios.create(), {
  maxRequests: 2,
  perMilliseconds: 1000,
  maxRPS: 1,
});

async function getCoordinates(address, country) {
  let params = {
    language: "fr",
    address: address,
  };
  if (country) {
    params.country = country;
  }
  let coord = await geocodingHttp({
    method: "GET",
    url: "https://trueway-geocoding.p.rapidapi.com/Geocode",
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host": "trueway-geocoding.p.rapidapi.com",
      "x-rapidapi-key": geocodingApiKey,
      useQueryString: true,
    },
    params: params,
  });
  return coord.data.results[0].location;
}

export default { getCoordinates };
