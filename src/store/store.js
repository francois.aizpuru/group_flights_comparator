import Vue from "vue";
import Vuex from "vuex";

import geocodingAPI from "../api/geocoding";
import flightAPI from "../api/flight";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    choosenAirports: [],
    choosenAirportsLocations: {},
    period: [],
    prices: [],
    step: 1,
  },
  mutations: {
    setChoosenAirports(state, choosenAirports) {
      state.choosenAirports = choosenAirports;
    },
    setPeriod(state, period) {
      state.period = period;
    },
    setStep(state, step) {
      state.step = step;
    },
  },
  getters: {
    getChoosenAirports: (state) => state.choosenAirports,
    getChoosenAirportsLocation: (state) => state.choosenAirportsLocations,
    getPeriod: (state) => state.period,
    getPrices: (state) => state.prices,
    getStep: (state) => state.step,
  },
  actions: {
    async setStep(context, step) {
      context.commit("setStep", step);
    },
    async setPeriod(context, period) {
      context.commit("setPeriod", period);
    },
    async setChoosenAirports(context, airports) {
      context.commit("setChoosenAirports", airports);

      let getCoordinates = async (airport) => {
        let result = {};

        let name = airport.split("(")[0].trim();
        var regExp = /\(([^)]+)\)/;
        let key = regExp.exec(airport)[1];
        if (this.state.choosenAirportsLocations[key]) {
          result[key] = this.state.choosenAirportsLocations[key];
          return result;
        }
        let country = key.split("-")[1].trim();

        result[key] = await geocodingAPI.getCoordinates(name, country);
        return result;
      };
      let coordinates = {};

      for (let airport of this.state.choosenAirports) {
        coordinates = { ...coordinates, ...(await getCoordinates(airport)) };
      }
      this.state.choosenAirportsLocations = coordinates;
    },
    async calculatePrices() {
      this.state.prices = [];
      var regExp = /\(([^)]+)\)/;

      let airportsToSearch = this.state.choosenAirports.map(
        (airport) => regExp.exec(airport)[1].split("-")[0] + "-sky"
      );

      let results = [];
      for (let airport of airportsToSearch) {
        results.push(await flightAPI.getQuotes(airport, this.state.period));
      }

      let placeIds = results
        .map((e1) => e1.map((e2) => e2.destination.PlaceId))
        .reduce((a, b) => a.filter((c) => b.includes(c)));

      let uniquePlacesIds = [...new Set(placeIds)];

      for (let placeId of uniquePlacesIds) {
        let place = results.map((el) =>
          el.filter((e2) => e2.destination.PlaceId == placeId)
        );
        let price = place.map((p) => p[0].minPrice).reduce((a, b) => a + b, 0);
        let address =
          place[0][0].destination.CityName +
          ", " +
          place[0][0].destination.CountryName;

        let coord = await geocodingAPI.getCoordinates(address);
        this.state.prices.push({
          price: price,
          coord: coord,
          address: address,
        });
      }
    },
  },
});
