import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import store from './store/store.js'

Vue.use(vuetify)
Vue.config.productionTip = false

// eslint-disable-next-line  
delete L.Icon.Default.prototype._getIconUrl  
// eslint-disable-next-line  
L.Icon.Default.mergeOptions({  
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),  
  iconUrl: require('leaflet/dist/images/marker-icon.png'),  
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')  
})

new Vue({
  vuetify,
  store,
  render: h => h(App),
  icons: {
    iconfont: 'mdi',
  }
}).$mount('#app')
